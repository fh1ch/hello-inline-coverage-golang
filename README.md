# hello-inline-coverage-golang

[![pipeline status](https://gitlab.com/fh1ch/hello-inline-coverage-golang/badges/main/pipeline.svg)](https://gitlab.com/fh1ch/hello-inline-coverage-golang/-/commits/main)

Golang example project to demonstrate the [GitLab Test Coverage Visualization feature](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html).

## Usage

Run:

```sh
go run .
```

Test:

```sh
go test .
```
