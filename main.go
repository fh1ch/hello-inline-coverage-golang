package main

import "fmt"

func run() {
	// With coverage
	if true {
		fmt.Println("Will be executed")
	}
}

func main() {
	run()
}
